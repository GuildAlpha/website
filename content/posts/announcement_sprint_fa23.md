---
title: Announcing the Second Guild Alpha Sprint
authors:
- CSDUMMI
- dannymate
- tomat0
tags:
- announcement
- guild
- sprint
- foss
- volunteer
- developers
categories:
- sprints
- gulid
- sprint2
date: 2023-09-13
---
We once again call for volunteer members in the Second Guild Alpha Sprint. We follow the same process as last time:
1. A proposal phase for gathering project proposals.
2. A vote between proposals among the members.
3. A development phase to implement the project choosen by vote.

If you want to take part in this, [join us here](https://cryptpad.fr/form/#/2/form/view/SolZncpOCId4S4DxlCP7RR6E4wxZ4VzNocAyjUVFoSE/).

## What's Guild Alpha?
Guild Alpha is an initiative of the Social Coding Movement. Every few months, we organize a sprint to gather people and organize our labour towards supporting free and open source software in a sustainable way.

## What's a Sprint?
A sprint should ideally be between 8 and 10 weeks and begins with this call for members. The members make proposals for projects to work on during the sprint, vote on the different proposals and then all members work on the proposal while supporting eachother.

During the entire sprint the editors, @tomat0, @dannymate and @CSDUMMI, will  create weekly progress reports, detailing the work achieved in the previous 7 days and defining tasks that they either identified themselves or another member suggested to them for the following 7 days.

After the weekly report has been published, members should notify the editors about the tasks they are willing undertake in the following week. The editors will reach about the progress on these tasks during the week and include any progress or problems encountered in the next weekly report - repeating the process.

You can read about our experience during the [first Guild Alpha Sprint]

## Make a proposal
You do not have to be a member of the Guild for this sprint, to make a proposal.
Anyone, who wants to suggest a project for us to develop this sprint, can do so through a [form](https://cryptpad.fr/form/#/2/form/view/O8AH4uK0-4S8OIAF0FnS9yi41ClYXhxNLjQOgKe3EmQ/). A [Matrix channel](https://matrix.to/#/!AxWrshSfuFNYStMaBZ:matrix.org?via=matrix.org) is open to discuss proposal ideas as well.

## Benefits for Members
This is an opportunity for members to work on free software in a team, learn a new skill or tech stack and engage more actively with the free software communities and it's projects.

## Benefits for Free Software
By bundling our energy together into a group, we can achieve more than if we all worked individually. The support provided by working in a group, the knowledge sharing and the quality control, means we can take on ambitious projects and actually succeed in them.

During this sprint we are only accepting proposals for features or large bug fixes on existing free software projects. All our work will be contributed back to the project in question. And our members will learn about the project and are thus more able to contribute in their own to any project we choose in this sprint.

## Who are the editors?
- [@tomat0](https://mastodon.social/@tomat0)
- [@dannymate](https://matrix.to/#/@dannymate:matrix.org)
- [@CSDUMMI](https://babka.social/@CSDUMMI)

